const parseStruct = require('./assets/scripts/parseStruct')

const express = require('express')
const cors = require('cors')
// const morgan = require('morgan')
// const requestIp = require('request-ip')
// const moment = require('moment')
const bodyParser = require('body-parser')
const service = {
  root: require('./service/root.js'),
  smm: require('./service/smm.js'),
  ping: require('./service/ping.js')
}

class ExpressServer {
  constructor () {
    this.expressApp = express()
    this.expressApp.use(cors())
    this.expressApp.use(bodyParser.json())
    // morgan.token('requestip', (req) => { return requestIp.getClientIp(req) })
    // morgan.token('custom_timestamp', () => { return moment(new Date()).format(`DD.MM.YYYY HH:mm:ss`) })
    // this.expressApp.use(
    //   morgan((tokens, req, res) => {
    //     return [
    //       tokens.status(req, res),
    //       tokens.method(req, res),
    //       `[${tokens.url(req, res)}]`,
    //       tokens.requestip(req, res),
    //       `(${tokens.res(req, res, 'content-length') ? tokens.res(req, res, 'content-length') : '?'} B)`,
    //       `{${tokens['response-time'](req, res)} ms}`, '|',
    //       tokens.custom_timestamp(req, res)
    //     ].join(' ')
    //   })
    // )

    this.data = {}
    this.data.endianness = 'little-endian'
    parseStruct().then((response) => {
      this.data.struct = [
        { length: 10, name: 'RESERVED' },
        ...response
      ]
    })

    this.attachRoutes()
  }

  attachRoutes () {
    const app = this.expressApp

    app.get(`/`, service.root.bind(this))
    app.post(`/smm`, service.smm.bind(this))
    app.post(`/ping`, service.ping.bind(this))
  }
}

module.exports = ExpressServer
