const fs = require('fs')

module.exports = function () {
  return new Promise((resolve, reject) => {
    fs.readFile('./src/main/express-server/assets/data/struct.c', 'utf8', (err, data) => {
      if (err) {
        reject(new Error(err))
      }
      const lines = data.split('\n')
      const struct = []
      const base = 4
      let readStruct = false
      lines.forEach((line) => {
        if (readStruct) {
          if (line.includes('}')) {
            readStruct = false
          } else {
            const curr = line.replace(/\s+/, ' ').trim().split(' ')
            let length = 1
            const match = curr[0].match(/\d+/)
            if (match) {
              length = parseInt(match[0]) / base
            }
            struct.push({
              length,
              name: curr[1].replace(';', '')
            })
          }
        } else {
          if (line.includes('{')) {
            readStruct = true
          }
        }
      })
      resolve(struct)
    })
  })
}
