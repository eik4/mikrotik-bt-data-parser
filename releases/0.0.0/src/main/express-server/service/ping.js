module.exports = function (req, res) {
  process.send({
    code: 'ping',
    message: req.body.message
  })
  res.json({ response: req.body.message })
}
