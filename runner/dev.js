const webpack = require('webpack')
const WebpackDevServer = require('webpack-dev-server')
const path = require('path')
const config = require('../webpack.config')
const { ESLint } = require('eslint')
const electron = require('electron')
const { spawn } = require('child_process')

const webpackPort = 8080

async function lint() {
  const reducer = (accumulator, curr) => {
    return accumulator + curr.messages.length
  }
  const eslint = new ESLint()
  const results = await eslint.lintFiles(['**'])
  const esLintErrors = results.filter((r) => { return r.errorCount > 0 })
  esLintErrors.forEach((result) => {
    if (result.errorCount > 0) {
      console.error('\n[ESLint] Error in', result.filePath)
      result.messages.forEach((message) => {
        console.error(`  (${message.line}:${message.column}) ${message.message} ${message.ruleId ? `{ ${message.ruleId} }` : ``}`)
      })
    }
  })
  console.log(`\nLinted ${results.length} files (${esLintErrors.length} invalid, ${esLintErrors.reduce(reducer, 0)} errors)`)
}

function startWebpack() {
  return new Promise((resolve, reject) => {
    try {
      const compiler = webpack(config)

      compiler.hooks.done.tap('done', (stats) => {
        lint()
        if (stats.compilation.hash) {
          console.log(`Hash: ${stats.compilation.hash}`)
        }
        console.log(`Assets: ${Object.entries(stats.compilation.assets).length}`)
        console.log(`Warnings: ${stats.compilation.warnings.length}`)
        stats.compilation.warnings.forEach((w) => {
          console.warn('[warning]', w)
        })
        console.log(`Errors: ${stats.compilation.errors.length}`)
        stats.compilation.errors.forEach((e) => {
          console.warn('[error]', e)
        })
        if (stats.startTime !== undefined && stats.endTime !== undefined) {
          console.log(`Compilation time: ${stats.endTime - stats.startTime} ms`)
        }
      })

      compiler.hooks.failed.tap('failed', (params) => {
        console.error('Compilation error')
        console.error(params)
      })

      // compiler.hooks.invalid.tap('invalid', (params) => {
      //   console.error()
      // })

      const server = new WebpackDevServer(
        compiler,
        {
          quiet: true
        }
      )

      server.listen(webpackPort)

      resolve(true)
    }
    catch {
      reject(new Error('Webpack error'))
    }
  })
}

function startElectron() {
  const electronProcess = spawn(electron, [path.join(__dirname, '../src/main/index.js')])

  electronProcess.stdout.on('data', (data) => {
    console.log('[Electron stdout start] =======')
    console.log(data.toString())
    console.log('[Electron stdout end] =========')
  })

  electronProcess.stderr.on('data', (data) => {
    console.log('[Electron stderr start] =======')
    console.log(data.toString())
    console.log('[Electron stderr end] =========')
  })

  electronProcess.on('close', () => {
    process.exit()
  })
}

process.env.RUNNER = 'dev'
process.env.WEBPACK_PORT = webpackPort

startWebpack().then(() => {
  startElectron()
}).catch((err) => {
  console.error('error', err)
})
