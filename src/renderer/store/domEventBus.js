const defaultState = {
  inited: false,
  event: {},
  prevent: {},
  origin: {}
}

export default {
  namespaced: true,

  state: () => ({
    ...defaultState
  }),

  mutations: {
    addOrigin(state, params) {
      if (state.origin[params.component] === undefined) {
        state.origin[params.component] = []
      }
      state.origin[params.component].push(params.handlerOrigin)
    },

    removeOrigin(state, params) {
      if (state.origin[params.component] !== undefined) {
        const index = state.origin[params.component].findIndex((handlerOrigin) => {
          return handlerOrigin.handler === params.handlerOrigin.handler &&
                 handlerOrigin.property === params.handlerOrigin.property &&
                 handlerOrigin.event === params.handlerOrigin.event
        })
        if (index !== -1) {
          state.origin[params.component].splice(index, 1)
        }
      }
    },

    addEvent(state, name) {
      if (state.event[name] === undefined) {
        state.event[name] = []
        if (state.inited) {
          this.dispatch('domEventBus/enableEvent', name)
        }
      }
    },

    addEventFirstHandler(state, params) {
      if (state.event[params.event] === undefined) {
        console.error(`[domEventBus/addEventFirstHandler] no event '${params.event}'`)
      } else {
        state.event[params.event].unshift(params.handler)
      }
    },

    addEventLastHandler(state, params) {
      if (state.event[params.event] === undefined) {
        console.error(`[domEventBus/addEventLastHandler] no event '${params.event}'`)
      } else {
        state.event[params.event].push(params.handler)
      }
    },

    removeEventHandler(state, params) {
      if (state.event[params.event] === undefined) {
        console.error(`[domEventBus/removeEventHandler] no such event '${params.event}'`)
      } else {
        const index = state.event[params.event].indexOf(params.handler)
        state.event[params.event].splice(index, 1)
      }
    },

    init(state) {
      state.inited = true
    },

    stop(state) {
      state.inited = false
    },

    clearState(state) {
      Object.keys(defaultState).forEach((key) => {
        state[key] = defaultState[key]
      })
    }
  },

  actions: {
    enableEvent(vuex, event) {
      if (document.body[event] === null) {
        const self = this
        document.body[event] = function (e) {
          vuex.state.event[event].forEach((handler) => {
            if (vuex.state.prevent[event]) {
              if (vuex.state.prevent[event][handler] !== true) {
                self.dispatch('vueEventBus/emit', { name: handler, arg: e })
              }
            } else {
              self.dispatch('vueEventBus/emit', { name: handler, arg: e })
            }
          })
        }
      } else {
        console.error(`[domEventBus/enableEvent] event '${event}' is already handling`)
      }
    },

    disableEvent(vuex, event) {
      if (document.body[event] === null) {
        console.error(`[domEventBus/disableEvent] event '${event}' is not handling`)
      } else {
        document.body[event] = null
      }
    },

    init(vuex) {
      for (const event in vuex.state.event) {
        vuex.dispatch('enableEvent', event)
      }
      vuex.commit('init')
    },

    stop(vuex) {
      if (vuex.state.inited) {
        for (const event in vuex.state.event) {
          vuex.dispatch('disableEvent', event)
        }
        vuex.commit('stop')
      } else {
        console.error('[domEventBus/stop] domEventBus is not inited')
      }
    },

    reset(vuex) {
      vuex.commit('stop')
      vuex.commit('clearState')
    },

    eventAddFirst(vuex, params) {
      vuex.commit('addEvent', params.event)
      vuex.commit('addEventFirstHandler', params)
      vuex.commit('addOrigin', {
        component: params.component,
        handlerOrigin: {
          event: params.event,
          handler: params.handler,
          property: 'event'
        }
      })
    },

    eventAddLast(vuex, params) {
      vuex.commit('addEvent', params.event)
      vuex.commit('addEventLastHandler', params)
      vuex.commit('addOrigin', {
        component: params.component,
        handlerOrigin: {
          event: params.event,
          handler: params.handler,
          property: 'event'
        }
      })
    },

    // todo: addBefore, addAfter

    eventHandlerRemove(vuex, params) {
      vuex.commit('removeEventHandler', params)
      vuex.commit('removeOrigin', {
        component: params.component,
        handlerOrigin: {
          event: params.event,
          handler: params.handler,
          property: 'event'
        }
      })
    },

    // todo: addPrevent, removePrevent

    componentDestroyed(vuex, component) {
      if (vuex.state.origin[component] === undefined) {
        console.error(`[domEventBus/componentDestroyed] component '${component}' has no handlers`)
      } else {
        vuex.state.origin[component].forEach((handlerOrigin) => {
          if (handlerOrigin.property === 'event') {
            vuex.commit('removeEventHandler', {
              event: handlerOrigin.event,
              handler: handlerOrigin.handler,
              component: component
            })
          } else {
            // todo: remove prevents
          }
          vuex.commit('removeOrigin', {
            component: component,
            handlerOrigin: handlerOrigin
          })
        })
      }
    }
  }
}
