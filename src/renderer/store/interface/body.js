const defaultState = {
  items: []
}

export default {
  namespaced: true,

  state: () => ({
    ...defaultState
  }),

  mutations: {
    addItem(state, item) {
      state.items.push(item)
    },

    removeItem(state, item) {
      const index = state.items.findIndex((i) => {
        return i === item
      })
      if (index !== -1) {
        state.items.splice(index, 1)
      }
    }
  },

  actions: { }
}
