import header from './header'
import body from './body'
import notifications from './notifications'

export default {
  namespaced: true,
  modules: {
    header,
    body,
    notifications
  }
}
