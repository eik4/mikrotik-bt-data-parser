const defaultState = {
  items: [],
  stopBurndown: false
}

export default {
  namespaced: true,

  state: () => ({
    ...defaultState
  }),

  mutations: {
    add(state, item) {
      state.items.unshift(item)
    },

    remove(state, id) {
      const index = state.items.findIndex((i) => {
        return i.id === id
      })
      if (index > -1) {
        state.items.splice(index, 1)
      }
    },

    burndown(state) {
      if (state.stopBurndown) {
        state.stopBurndown = false
      } else {
        if (state.items.length > 0) {
          const toDelete = []
          for (let itemIndex = 0; itemIndex < state.items.length; itemIndex++) {
            if (state.items[itemIndex].hovered === false) {
              state.items[itemIndex].health -= 1
              if (state.items[itemIndex].health < 0) {
                toDelete.unshift(itemIndex)
              }
            }
          }
          toDelete.forEach((d) => {
            state.items.splice(d, 1)
          })
          if (state.items.length > 0) {
            requestAnimationFrame(() => {
              this.commit('interface/notifications/burndown')
            })
          }
        }
      }
    },

    stopBurndown(state) {
      state.stopBurndown = true
    },

    hover(state, id) {
      const item = state.items.find((i) => {
        return i.id === id
      })
      if (item) {
        item.hovered = true
      }
    },

    blur(state, id) {
      const item = state.items.find((i) => {
        return i.id === id
      })
      if (item) {
        item.hovered = false
      }
    }
  },

  actions: {
    add(vuex, params = {}) {
      const defaults = {
        type: 'info',
        header: 'Info',
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna...',
        duration: 1000,
        health: 1000,
        hovered: false
      }
      const settings = { ...defaults, ...params }
      let id = new Date().toISOString()
      let find = vuex.state.items.find((item) => { return item.id === id })
      while (find) {
        id = `${id}_`
        find = vuex.state.items.find((item) => { return item.id === id })
      }
      vuex.commit('add', { id, ...settings })
      if (vuex.state.items.length < 2) {
        vuex.commit('burndown')
      }
    }
  }
}
