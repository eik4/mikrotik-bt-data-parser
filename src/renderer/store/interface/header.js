const defaultState = {
  connection: {
    icon: 'Router',
    color: '#ccc',
    text: 'Не подключен',
    active: false,
    component: 'Connection'
  },
  data: {
    icon: 'Timeline',
    color: '#ccc',
    text: 'Нет данных',
    active: false,
    component: 'Data'
  }
}

export default {
  namespaced: true,

  state: () => ({
    ...defaultState
  }),

  mutations: {
    toggle(state, item) {
      if (state[item] !== undefined) {
        state[item].active = !state[item].active
      }
    },

    // params: {
    //   item: String,
    //   param: String
    //   value: Any
    // }
    setItemParam(state, params) {
      if (state[params.item] !== undefined) {
        if (state[params.item][params.param] !== undefined) {
          state[params.item][params.param] = params.value
        }
      }
    }
  },

  actions: { }
}
