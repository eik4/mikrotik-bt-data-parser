const defaultState = {
  webServer: {
    started: false,
    starting: false,
    host: null,
    port: null
  }
}

export default {
  namespaced: true,

  state: () => ({
    ...defaultState
  }),

  mutations: {
    setWebServer(state, webServer) {
      if (webServer.host === null || webServer.port === null) {
        state.webServer.host = null
        state.webServer.port = null
        state.webServer.started = false
        this.commit('interface/header/setItemParam', {
          item: 'connection',
          param: 'text',
          value: 'Не подключен'
        })
        this.commit('interface/header/setItemParam', {
          item: 'connection',
          param: 'color',
          value: '#ccc'
        })
      } else {
        state.webServer.host = webServer.host
        state.webServer.port = webServer.port
        state.webServer.started = true
        this.commit('interface/header/setItemParam', {
          item: 'connection',
          param: 'text',
          value: `${webServer.host}:${webServer.port}`
        })
        this.commit('interface/header/setItemParam', {
          item: 'connection',
          param: 'color',
          value: '#99ff99'
        })
      }
    }
  },

  actions: { }
}
