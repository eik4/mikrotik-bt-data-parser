import app from './app'
import domEventBus from './domEventBus'
import vueEventBus from './vueEventBus'
import interfaceIndex from './interface/index'

export default {
  modules: {
    app,
    domEventBus,
    vueEventBus,
    interface: interfaceIndex
  }
}
