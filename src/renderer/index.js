import Vue from 'vue'
import Vuex from 'vuex'
import App from './components/App'
import storeModules from './store/index'
import './index.scss'
import { ipcRenderer } from 'electron'

Vue.use(Vuex)

const store = new Vuex.Store(storeModules)

new Vue({
  el: '#app',

  store,

  components: {
    App
  },

  async created() {
    ipcRenderer.on('message-from-ping', (event, ...args) => {
      this.$store.dispatch('interface/notifications/add', {
        header: 'Получено сообщение',
        text: args[0]
      })
    })
    const response = await ipcRenderer.invoke('main-getInitialData')
    this.$store.commit('app/setWebServer', response.webServer)
  },

  mounted() {
    this.$store.dispatch('domEventBus/init')
  },

  beforeDestroy() {
    this.$store.dispatch('domEventBus/reset')
  }
})
