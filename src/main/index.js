const electron = require('electron')

const appState = {
  handlers: {
    main: require('./handlers/main/index'),
    'web-server': require('./handlers/web-server/index')
  },
  webServer: {
    host: null,
    port: null,
    child: null
  },
  win: null
}

function createWindow () {
  const win = new electron.BrowserWindow({
    width: 1280,
    height: 720,
    webPreferences: {
      nodeIntegration: true
    }
  })

  if (process.env.RUNNER === 'dev') {
    win.loadURL(`http://localhost:${process.env.WEBPACK_PORT}`)
    win.webContents.openDevTools()
  }
  else if (process.env.RUNNER === 'electron') {
    win.loadFile('../../dist/index.html')
  }
  else {
    win.loadFile('./dist/index.html')
  }

  win.setMenu(null)

  appState.win = win
}

for (const [handlerKey, handlerValues] of Object.entries(appState.handlers)) {
  for (const [handlerName, handler] of Object.entries(handlerValues)) {
    electron.ipcMain.handle(`${handlerKey}-${handlerName}`, handler.bind(appState))
  }
}

const app = electron.app

app.on('ready', () => {
  createWindow()
})

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})
