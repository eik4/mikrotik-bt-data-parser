module.exports = function (req, res) {
  const response = {
    message: '',
    status: ''
  }
  if (this.data.struct) {
    let structIndex = 0
    let inputIndex = 0
    const input = req.body.msg
    while (inputIndex < input.length) {
      const curr = this.data.struct[structIndex]
      let hex = ''
      while (hex.length < curr.length) {
        hex = `${hex}${input[inputIndex]}`
        inputIndex += 1
      }
      console.log(`${curr.name}: 0x${hex} (${parseInt(`0x${hex}`)})`)
      structIndex += 1
    }
  } else {
    console.log('Struct is not yet parsed')
  }
  res.json(response)
}
