const ExpressServer = require('./app')
// const config = require('./config.json')

const app = new ExpressServer()
const host = process.argv[2]
const port = parseInt(process.argv[3])
app.expressApp.listen(port, host, () => {
  process.send({ code: 'started' })
})
