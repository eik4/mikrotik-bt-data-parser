const test = require('./test')
const getInitialData = require('./getInitialData')

module.exports = {
  test,
  getInitialData
}
