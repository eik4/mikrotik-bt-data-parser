module.exports = function () {
  return new Promise((resolve, reject) => {
    this.webServer.child.on('close', () => {
      this.webServer.host = null
      this.webServer.port = null
      this.webServer.child = null
      resolve()
    })
    this.webServer.child.kill('SIGINT')
    // fix this reject
    setTimeout(() => {
      reject(new Error('NO'))
    }, 10000)
  })
}
