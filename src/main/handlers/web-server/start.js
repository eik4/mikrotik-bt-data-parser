const { fork } = require('child_process')

module.exports = function (...args) {
  return new Promise((resolve) => {
    const host = args[1]
    const port = args[2]
    const webServerFile = `${process.cwd()}/src/main/express-server/server.js`
    const child = fork(webServerFile, [host, port])
    child.on('spawn', () => {
      resolve({ host, port: parseInt(port) })
    })
    child.on('message', (message) => {
      switch (message.code) {
        case 'started':
          this.webServer.host = host
          this.webServer.port = port
          this.webServer.child = child
          resolve({ host, port: parseInt(port) })
          break
        case 'ping':
          this.win.webContents.send('message-from-ping', message.message)
          break
        default:
          break
      }
    })
  })
}
